Web hooks for legolas.paleogene.net
###################################

Flask-based web hooks for legolas.paleogene.net.  Currently implements a
Pelican site update script for markshroyer.com, which is triggered when the
master branch is updated on my private Bitbucket repository for the web
site.

