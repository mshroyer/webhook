#!/bin/sh

# Fetches the latest version of the website from Bitbucket, rebuilds with
# Pelican, and then uploads to Amazon S3.
#
# Mark Shroyer
# Mon Apr  7 19:23:21 EDT 2014

set -e

HOME="/var/lib/nginx"
export HOME

PATH="/opt/env/pelican.nginx/bin:$PATH"
export PATH

PIP_REQS=requirements.txt

cd /opt/markshroyer.com

old_head=$(git rev-parse HEAD)
git pull
new_head=$(git rev-parse HEAD)

if [ $old_head != $new_head ]; then
    if [ -f "$PIP_REQS" ]; then
        pip install -r "$PIP_REQS"
    fi
    make s3_upload
else
    echo "HEAD not updated; skipping s3_upload"
fi
