#!/usr/bin/env python

"""webhook

Implements web hooks for BitBucket, etc. on my Amazon EC2 instance.

"""

from __future__ import print_function

__author__ = 'Mark Shroyer'

from flask import Flask
from flask import request
from flask import json

import subprocess
from datetime import datetime, timedelta

class WebHookThrottle(object):
    """Web hook throttle

    Allows us to impose a throttle on our S3 uploads so we don't get hit
    with a huge bill if Bitbucket loses its mind...

    """

    def __init__(self, period=timedelta(hours=1), threshold=5):
        self.period = period
        self.threshold = threshold
        self.events = []

    def submit(self):
        now = datetime.utcnow()
        self.events = filter(lambda evt: now - evt <= self.period, self.events)
        if len(self.events) < self.threshold:
            self.events.append(now)
        else:
            raise Exception("Web hook throttle threshold exceeded!")

def create_app():
    app = Flask(__name__)
    app.debug = True
    return app

app = create_app()
pelican_throttle = WebHookThrottle(period=timedelta(hours=1), threshold=30)

@app.route('/pelican', methods=['POST'])
def pelican():
    # Make sure the request actually came from Bitbucket
    if request.remote_addr not in ["131.103.20.165", "131.103.20.166"]:
        raise Exception("Invalid source IP address for pelican hook")

    ua = request.headers['User-Agent']
    if ua != "Bitbucket.org":
        raise Exception("Unexpected User-Agent for pelican hook: {0}".format(ua))

    request_json = json.loads(request.form.get('payload'))

    # Check whether we're even dealing with the right repository
    repo = request_json['repository']['absolute_url']
    if repo != '/markshroyer/markshroyerdotcom/':
        raise Exception("Wrong repository for pelican hook: {0}".format(repo))

    # Don't need to do anything if master wasn't touched
    on_master = False
    for commit in request_json['commits']:
        if commit['branch'] == 'master':
            on_master = True
            break

    if not on_master:
        app.logger.info("master branch not updated, ignoring pelican hook")
        return ("", 200)

    pelican_throttle.submit()
    app.logger.debug("Invoking pelican hook shell script")
    subprocess.check_call("/opt/webhook/pelican.sh", shell=True)

    return ("", 200)

@app.route('/')
def root():
    return "Web hook manager"

if __name__ == '__main__':
    app.run()
